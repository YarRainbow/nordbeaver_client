import "./ObjectExtension";
import { Game } from "./by/nordbeaver/Game";
import WebFont from "webfontloader";

const isMobile = /Mobi/i.test(window.navigator.userAgent);
const documentParent = document.getElementById('game');
const game = new Game(isMobile);
function start() {
    documentParent.appendChild(game.view);
    game.launch();
}

let webFontConfig = {
    custom: {
        families: ['Regular'],
        urls: ['css/fonts.css']
    }
};

WebFont.load(webFontConfig);
start();