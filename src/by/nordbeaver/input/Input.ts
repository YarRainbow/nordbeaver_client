import MiniSignal from "mini-signals";
import { KeyCode } from "./KeyCode";

export class Input {
    public readonly jumpSignal: MiniSignal;

    public constructor() {
        this.jumpSignal = new MiniSignal();
        document.addEventListener("keydown", this.keyPressed.bind(this), false);
    }

    private keyPressed(event: KeyboardEvent): void {
        if (event.code == KeyCode.Jump) {
            this.jumpSignal.dispatch();
        }
    }
}