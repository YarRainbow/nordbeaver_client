import { Application, Container } from "pixi.js";
import MiniSignal from 'mini-signals';
import { SceneManager } from "./scene/SceneManager";
import { DummyTransition } from "./scene/transition/DummyTransition";
import { DummyItemFactory } from "./scene/transition/DunnyItemFactory";
import { SafeAreaProvider } from "./testproject/tools/SafeAreasProvider";
import { PreloaderScene } from "./testproject/scene/preloaderScene/PreloaderScene";
import { PreloaderConfigModel } from "./testproject/scene/preloaderScene/model/PreloaderConfigModel";
import { ResourcesConfigModel } from "./testproject/scene/preloaderScene/model/ResourcesConfigModel";
import { NetworkClient } from "./testproject/network/NetworkClient";
import { RootModel } from "./testproject/rootModel/RootModel";
import { GameScene } from "./testproject/scene/gameScene/GameScene";
import { SoundManager } from "./testproject/soundManager/SoundManager";
import { StartupConfig } from "./configs/StartupConfig";

export class Game extends Application {
    public static readonly WIDTH: number = 1920;
    public static readonly HEIGHT: number = 1080;
    public readonly onUpdate: MiniSignal;
    private readonly safeArea: number;
    private readonly soundManager: SoundManager;
    private gameContainer: Container;
    private sceneManager: SceneManager;

    public constructor(isMobile: boolean) {
        super({
            backgroundColor: 0xFFFFFF
        });
        this.onUpdate = new MiniSignal();
        this.safeArea = SafeAreaProvider.getSafeArea();
        this.soundManager = new SoundManager();
        this.ticker.add(this.dispatchUpdateEvent, this);
    }

    public async launch(): Promise<void> {
        this.gameContainer = new Container();
        this.gameContainer.pivot.set(Game.WIDTH / 2, Game.HEIGHT / 2);
        this.stage.addChild(this.gameContainer);
        this.sceneManager = new SceneManager(this.gameContainer, new DummyTransition(new DummyItemFactory(), 0));
        window.onresize = () => { this.onRendererResize(window.innerWidth, window.innerHeight) };
        window.addEventListener("orientationchange", () => {
            window.dispatchEvent(new Event("resize"));
        }, false);
        this.onRendererResize(window.innerWidth, window.innerHeight);
        this.showPreloader();
    }

    private onRendererResize(screenWidth: number, screenHeight: number): void {
        const width = screenWidth * devicePixelRatio;
        const height = screenHeight * devicePixelRatio;
        this.renderer.resize(width, height);
        this.view.style.transform = `scale(${1 / devicePixelRatio})`;
        this.scaleContainer(this.gameContainer, width, height, Game.WIDTH, Game.HEIGHT);
        document.documentElement.style.setProperty('--app-height', `${window.innerHeight}px`);
    }

    private scaleContainer(container: Container, realWidth: number, realHeight: number, desiredWidth: number, desiredHeight: number): void {
        const scaleFactor = Math.min(
            realWidth / (desiredWidth - this.safeArea),
            realHeight / (desiredHeight - this.safeArea));
        container.scale.set(scaleFactor, scaleFactor);
        container.position.set(this.screen.width / 2, this.screen.height / 2);
    }

    private async showPreloader(): Promise<void> {
        const preloaderScene = await PreloaderScene.build();
        const preloaderSceneOperation = this.sceneManager.showViewableAsync(preloaderScene);
        preloaderSceneOperation.then(async (preloaderConfigModel: PreloaderConfigModel) => {
            this.showGame(preloaderConfigModel.resources, preloaderConfigModel.networkClient, preloaderConfigModel.startupConfig);
        });
    }

    private async showGame(resources: ResourcesConfigModel, networkClient: NetworkClient, startupConfig: StartupConfig): Promise<void> {
        const rootModel = new RootModel(networkClient);
        const gameScene = await GameScene.build(resources.gameSceneResourcePackage, resources.gameSceneUiResourcePackage, networkClient, this.sceneManager, this.soundManager, rootModel.playerModel, rootModel.gameStateModel, startupConfig);
        this.sceneManager.showViewableAsync(gameScene);
    }

    private dispatchUpdateEvent(dt: number): void {
        this.onUpdate.dispatch(this.ticker.elapsedMS);
    }
}