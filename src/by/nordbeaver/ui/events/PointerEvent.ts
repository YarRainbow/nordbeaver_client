export enum PointerEvent {
	Down = 'pointerdown',
	Up = 'pointerup',
	UpOutside = 'pointerupoutside',
	Move = 'pointermove',
	Over = 'pointerover',
	Out = 'pointerout'
}
