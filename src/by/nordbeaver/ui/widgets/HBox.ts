import { VBox } from "./VBox";

export class HBox extends VBox {
    
    public override alignmentStrategy() {
        let positionX = 0.0;
        for (let i = 0; i < this.children.length; i++) {
            let child = this.children[i];
            child.position.x = positionX;
            positionX += child.getLocalBounds().width + this.childSpacing;
        }
    }    
}