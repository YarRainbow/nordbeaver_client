import { Sprite, Text, TextStyle } from "pixi.js";
import { Game } from "../../../Game";
import { SceneManager } from "../../../scene/SceneManager";
import { BaseView } from "../../../scene/viewable/BaseView";
import { Viewable, ViewableOperationControls } from "../../../scene/viewable/Viewable";
import { PointerEvent } from "../../../ui/events/PointerEvent";
import { PlayerModel } from "../../scene/gameScene/components/player/model/PlayerModel";
import { GameSceneUiResourcePackage } from "../../scene/gameScene/resources/GameSceneUiResourcePackage";
import { SoundManager } from "../../soundManager/SoundManager";
import { HighScorePopup } from "../highScorePopup/HighScorePopup";
import { CoinsLabel } from "./components/CoinsLabel";
import { DistanceLabel } from "./components/DistanceLabel";
import { OkButton } from "./components/OkButton";
import { ScoreLabel } from "./components/ScoreLabel";

export class ScorePopup extends Viewable<void> {
    public constructor(gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager, sceneManager: SceneManager, playerModel: PlayerModel) {
        super();
        const scorePopupController = new ScorePopupController(this.operationControls, sceneManager, gameSceneUiResourcePackage, soundManager, playerModel);
        this.view = new ScorePopupView(gameSceneUiResourcePackage, scorePopupController, soundManager);
    }
}

class ScorePopupController {
    private readonly operationControls: ViewableOperationControls<void>;
    private readonly sceneManager: SceneManager;
    private readonly gameSceneUiResourcePackage: GameSceneUiResourcePackage;
    private readonly soundManager: SoundManager;
    private readonly playerModel: PlayerModel;

    public constructor(operationControls: ViewableOperationControls<void>, sceneManager: SceneManager, gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager, playerModel: PlayerModel) {
        this.operationControls = operationControls;
        this.sceneManager = sceneManager;
        this.gameSceneUiResourcePackage = gameSceneUiResourcePackage;
        this.soundManager = soundManager;
        this.playerModel = playerModel;
    }

    public onOkButtonClick(): void {
        this.operationControls.complete();
        const highScorePopup = new HighScorePopup(this.gameSceneUiResourcePackage, this.sceneManager, this.soundManager, this.playerModel);
        this.sceneManager.showViewableAsync(highScorePopup);
    }
}

class ScorePopupView extends BaseView {

    private readonly gameSceneUiResourcePackage: GameSceneUiResourcePackage;
    private readonly scorePopupController: ScorePopupController;
    private readonly soundManager: SoundManager;
    private readonly uiElementsLeftIndent: number = 100;

    public constructor(gameSceneUiResourcePackage: GameSceneUiResourcePackage, scorePopupController: ScorePopupController, soundManager: SoundManager) {
        super();
        this.gameSceneUiResourcePackage = gameSceneUiResourcePackage;
        this.scorePopupController = scorePopupController;
        this.soundManager = soundManager;
        this.pivot.set(Game.WIDTH / 2, Game.HEIGHT / 2);
        this.position.set(Game.WIDTH / 2, Game.HEIGHT / 2);
        this.start();
    }

    public start(): void {
        const frame = new Sprite(this.gameSceneUiResourcePackage.info_plate_big);
        frame.anchor.set(0.5, 0.5);
        frame.position.set(Game.WIDTH / 2, Game.HEIGHT / 2);
        this.addChild(frame);

        const header = new Sprite(this.gameSceneUiResourcePackage.header_info_plate);
        header.anchor.set(0.5, 0);
        header.y -= frame.height / 2;
        frame.addChild(header);

        const headerText = new Text("Твои очки:", new ScorePopupHeaderTextStyle());
        headerText.anchor.set(0.5, 0.5);
        headerText.y += header.height / 2;
        header.addChild(headerText);

        const scoreLabel = new ScoreLabel();
        scoreLabel.anchor.set(0.5, 0);
        scoreLabel.y = -frame.height / 2 + 120;
        frame.addChild(scoreLabel);

        const coinIcon = new Sprite(this.gameSceneUiResourcePackage.collect_coin_icon);
        coinIcon.anchor.set(0, 1);
        coinIcon.x = -frame.width / 2 + this.uiElementsLeftIndent;
        frame.addChild(coinIcon);

        const coinsLabel = new CoinsLabel();
        coinsLabel.anchor.set(0.5, 0.5);
        coinsLabel.y = coinIcon.y - coinIcon.height / 2;
        frame.addChild(coinsLabel);

        const distanceIcon = new Sprite(this.gameSceneUiResourcePackage.collect_distance_icon);
        distanceIcon.x = -frame.width / 2 + this.uiElementsLeftIndent;
        distanceIcon.y = distanceIcon.height / 2;
        frame.addChild(distanceIcon);

        const distanceLabel = new DistanceLabel();
        distanceLabel.anchor.set(0.5, 0.5);
        distanceLabel.y = distanceIcon.y + distanceIcon.height / 2;
        frame.addChild(distanceLabel);

        const okButton = new OkButton(frame, this.gameSceneUiResourcePackage, this.soundManager);
        okButton.anchor.set(0.5, 1);
        okButton.y = frame.height / 2 - okButton.height / 2;
        okButton.on(PointerEvent.Up, this.scorePopupController.onOkButtonClick, this.scorePopupController);
    }
}

class ScorePopupHeaderTextStyle extends TextStyle {
    public constructor() {
        super({
            align: "center",
            fontFamily: "Regular",
            fontSize: 50,
            fill: 0x003d71
        });
    }
}