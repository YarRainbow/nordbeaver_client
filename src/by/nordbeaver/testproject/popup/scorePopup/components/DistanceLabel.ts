import { Text, TextStyle } from "pixi.js"

export class DistanceLabel extends Text {
    public constructor() {
        super("80 м", new CoinsLabelTextStyle());
    }
}

class CoinsLabelTextStyle extends TextStyle {
    public constructor() {
        super({
            align: "center",
            fontFamily: "Regular",
            fontSize: 90,
            fill: 0x9ac6ff,
            dropShadow: true,
            dropShadowDistance: 5,
            dropShadowAlpha: 0.5
        });
    }
}