import { Text, TextStyle } from "pixi.js"

export class ScoreLabel extends Text {
    public constructor() {
        super("100", new ScoreLabelTextStyle());
    }
}

class ScoreLabelTextStyle extends TextStyle {
    public constructor() {
        super({
            align: "center",
            fontFamily: "Regular",
            fontSize: 150,
            fill: 0x00cc00,
            dropShadow: true,
            dropShadowDistance: 5,
            dropShadowAlpha: 0.5
        });
    }
}