import { Sprite, Text, TextStyle } from "pixi.js";
import { Game } from "../../../Game";
import { SceneManager } from "../../../scene/SceneManager";
import { BaseView } from "../../../scene/viewable/BaseView";
import { Viewable, ViewableOperationControls } from "../../../scene/viewable/Viewable";
import { PointerEvent } from "../../../ui/events/PointerEvent";
import { PlayerModel } from "../../scene/gameScene/components/player/model/PlayerModel";
import { GameSceneUiResourcePackage } from "../../scene/gameScene/resources/GameSceneUiResourcePackage";
import { SoundManager } from "../../soundManager/SoundManager";
import { HighScorePopup } from "../highScorePopup/HighScorePopup";
import { ScorePopup } from "../scorePopup/ScorePopup";
import { LeadboardButton } from "./components/LeadboardButton";
import { LoginButton } from "./components/LoginButton";
import { PlayButton } from "./components/PlayButton";
import { RecordLabel } from "./components/RecordLabel";
import { UserNameBar } from "./components/UserNameBar";

export class StartGamePopup extends Viewable<void> {
    public constructor(gameSceneUiResourcePackage: GameSceneUiResourcePackage, sceneManager: SceneManager, soundManager: SoundManager, playerModel: PlayerModel) {
        super();
        const startGamePopupController = new StartGamePopupController(this.operationControls, sceneManager, gameSceneUiResourcePackage, soundManager, playerModel);
        this.view = new StartGamePopupView(gameSceneUiResourcePackage, startGamePopupController, soundManager, playerModel);
    }
}

class StartGamePopupController {
    private readonly operationControls: ViewableOperationControls<void>;
    private readonly sceneManager: SceneManager;
    private readonly gameSceneUiResourcePackage: GameSceneUiResourcePackage;
    private readonly soundManager: SoundManager;
    private readonly playerModel: PlayerModel;

    public constructor(operationControls: ViewableOperationControls<void>, sceneManager: SceneManager, gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager, playerModel: PlayerModel) {
        this.operationControls = operationControls;
        this.sceneManager = sceneManager;
        this.gameSceneUiResourcePackage = gameSceneUiResourcePackage;
        this.soundManager = soundManager;
        this.playerModel = playerModel;
    }

    public onPlayButtonClick(): void {
        this.operationControls.complete();
        const startGamePopup = new ScorePopup(this.gameSceneUiResourcePackage, this.soundManager, this.sceneManager, this.playerModel);
        this.sceneManager.showViewableAsync(startGamePopup);
    }

    public onLeadButtonClick(): void {
        this.operationControls.complete();
        const highScorePopup = new HighScorePopup(this.gameSceneUiResourcePackage, this.sceneManager, this.soundManager, this.playerModel);
        this.sceneManager.showViewableAsync(highScorePopup);
    }
}

class StartGamePopupView extends BaseView {

    private readonly gameSceneUiResourcePackage: GameSceneUiResourcePackage;
    private readonly startGamePopupController: StartGamePopupController;
    private readonly soundManager: SoundManager;
    private readonly uiElementsBottomIndent: number = 50;
    private readonly uiElementsLeftIndent: number = 30;
    private readonly playerModel: PlayerModel;

    public constructor(gameSceneUiResourcePackage: GameSceneUiResourcePackage, startGamePopupController: StartGamePopupController, soundManager: SoundManager, playerModel: PlayerModel) {
        super();
        this.gameSceneUiResourcePackage = gameSceneUiResourcePackage;
        this.startGamePopupController = startGamePopupController;
        this.soundManager = soundManager;
        this.playerModel = playerModel;
        this.pivot.set(Game.WIDTH / 2, Game.HEIGHT / 2);
        this.position.set(Game.WIDTH / 2, Game.HEIGHT / 2);
        this.start();
    }

    public start(): void {
        const frame = new Sprite(this.gameSceneUiResourcePackage.info_plate_big);
        frame.anchor.set(0.5, 0.5);
        frame.position.set(Game.WIDTH / 2, Game.HEIGHT / 2);
        this.addChild(frame);

        const recordLabel = new RecordLabel();
        recordLabel.anchor.set(0.5, 0);
        recordLabel.y = -frame.height / 2 + 120;
        frame.addChild(recordLabel);

        const header = new Sprite(this.gameSceneUiResourcePackage.header_info_plate);
        header.anchor.set(0.5, 0);
        header.y -= frame.height / 2;
        frame.addChild(header);

        const headerText = new Text("Твои рекорды:", new StartGamePopupHeaderTextStyle());
        headerText.anchor.set(0.5, 0.5);
        headerText.y += header.height / 2;
        header.addChild(headerText);

        const leadboardButton = new LeadboardButton(frame, this.gameSceneUiResourcePackage, this.soundManager);
        leadboardButton.anchor.set(0, 1);
        leadboardButton.x = -frame.width / 2 + this.uiElementsLeftIndent;
        leadboardButton.y = frame.height / 2 - this.uiElementsBottomIndent;
        leadboardButton.on(PointerEvent.Up, this.startGamePopupController.onLeadButtonClick, this.startGamePopupController);

        const playButton = new PlayButton(frame, this.gameSceneUiResourcePackage, this.soundManager);
        playButton.anchor.set(1, 1);
        playButton.x = frame.width / 2 - this.uiElementsLeftIndent;
        playButton.y = frame.height / 2 - this.uiElementsBottomIndent;
        playButton.on(PointerEvent.Up, this.startGamePopupController.onPlayButtonClick, this.startGamePopupController);

        const loginButton = new LoginButton(frame, this.gameSceneUiResourcePackage, this.soundManager);
        loginButton.anchor.set(0.5, 1);

        const userNameBar = new UserNameBar(this.gameSceneUiResourcePackage, this.playerModel);
        userNameBar.anchor.set(0.5, 0);
        userNameBar.y += userNameBar.height / 2;
        frame.addChild(userNameBar);
    }
}

class StartGamePopupHeaderTextStyle extends TextStyle {
    public constructor() {
        super({
            align: "center",
            fontFamily: "Regular",
            fontSize: 50,
            fill: 0x003d71,
        });
    }
}