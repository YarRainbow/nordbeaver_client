import { Container, TextStyle, Text, Texture, Sprite, TextStyleFill } from "pixi.js";
import { PointerEvent } from "../../../../ui/events/PointerEvent";
import { GameSceneUiResourcePackage } from "../../../scene/gameScene/resources/GameSceneUiResourcePackage";
import { SoundManager } from "../../../soundManager/SoundManager";
import { ArrowButton } from "./ArrowButton";

export class LeadboardSlider extends Container {
    private readonly distanceBetweenArrows: number = 250;
    private readonly defaultItemIndentLeft: number = 70;
    private readonly prizeItemIndentLeft: number = 100;
    private readonly firstItemPositionY: number = 100;
    private readonly gameSceneUiResourcePackage: GameSceneUiResourcePackage;
    private readonly soundManager: SoundManager;
    private readonly statesNames: Map<LeadboardSliderStates, string>;
    private readonly states: Map<LeadboardSliderStates, Record[]>;
    private readonly leaderBoardItems: Sprite[];
    private leadboardLabel: Text;
    private currentStateIndex: number = LeadboardSliderStates.All;

    public constructor(gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager) {
        super();
        this.gameSceneUiResourcePackage = gameSceneUiResourcePackage;
        this.soundManager = soundManager;
        this.statesNames = new Map([
            [LeadboardSliderStates.All, "Все время"],
            [LeadboardSliderStates.Month, "Месяц"],
            [LeadboardSliderStates.Week, "Неделя"]
        ]);
        const testRecords = new TestRecords();
        this.states = new Map([
            [LeadboardSliderStates.All, testRecords.allTimeRecords],
            [LeadboardSliderStates.Month, testRecords.monthRecords],
            [LeadboardSliderStates.Week, testRecords.weekRecords]
        ]);
        this.leaderBoardItems = new Array<Sprite>();
        this.create();
    }

    private create(): void {
        const leftArrowButton = new ArrowButton(this, this.gameSceneUiResourcePackage, this.soundManager);
        leftArrowButton.scale.x = -1;
        leftArrowButton.x -= this.distanceBetweenArrows;
        leftArrowButton.on(PointerEvent.Up, this.onLeftArrowClick, this);

        const rightArrowButton = new ArrowButton(this, this.gameSceneUiResourcePackage, this.soundManager);
        rightArrowButton.x += this.distanceBetweenArrows;
        rightArrowButton.on(PointerEvent.Up, this.onRightArrowClick, this);

        this.leadboardLabel = new Text(this.statesNames.get(this.currentStateIndex), new LeadboardSliderTextStyle());
        this.leadboardLabel.anchor.x = 0.5;
        this.addChild(this.leadboardLabel);

        this.renderRecords(this.states.get(this.currentStateIndex));
    }

    private renderRecords(records: Record[]): void {
        this.clearOldRecords();
        let previousLeaderboardItem: Sprite;
        let leaderboardItem: Sprite;
        for (let i = 0; i < records.length; i++) {
            if (i <= PrizeLeaderboardItemType.Bronze) {
                leaderboardItem = new PrizeLeaderboardItem(this.gameSceneUiResourcePackage, i, records[i].name, records[i].score);
                leaderboardItem.x = -this.prizeItemIndentLeft;
            }
            else {
                leaderboardItem = new DefaultLeaderboardItem(this.gameSceneUiResourcePackage, i + 1, records[i].name, records[i].score);
                leaderboardItem.x = -this.defaultItemIndentLeft;
            }
            leaderboardItem.y = previousLeaderboardItem ? previousLeaderboardItem.y + previousLeaderboardItem.height / 2 + leaderboardItem.height / 2 + 10 : this.firstItemPositionY;
            previousLeaderboardItem = this.addChild(leaderboardItem);
            this.leaderBoardItems[i] = this.addChild(leaderboardItem);
        }
    }

    private clearOldRecords(): void {
        for (let i = 0; i < this.leaderBoardItems.length; i++) {
            this.removeChild(this.leaderBoardItems[i]);
        }
    }

    private onRightArrowClick(): void {
        const nextStateIndex = this.currentStateIndex + 1;
        this.currentStateIndex = nextStateIndex >= this.statesNames.size ? 0 : nextStateIndex;
        this.leadboardLabel.text = this.statesNames.get(this.currentStateIndex);
        this.renderRecords(this.states.get(this.currentStateIndex));
    }

    private onLeftArrowClick(): void {
        const previousStateIndex = this.currentStateIndex - 1;
        this.currentStateIndex = previousStateIndex < 0 ? this.statesNames.size - 1 : previousStateIndex;
        this.leadboardLabel.text = this.statesNames.get(this.currentStateIndex);
        this.renderRecords(this.states.get(this.currentStateIndex));
    }
}

class DefaultLeaderboardItem extends Sprite {
    private readonly gameSceneUiResourcePacakge: GameSceneUiResourcePackage;
    private readonly elementsOffset: number = 20;

    public constructor(gameSceneUiResourcePacakge: GameSceneUiResourcePackage, place: number, name: string, score: number) {
        super(gameSceneUiResourcePacakge.midleader_name_plate);
        this.gameSceneUiResourcePacakge = gameSceneUiResourcePacakge;
        this.anchor.set(0.5, 0.5);
        this.create(place, name, score);
    }

    private create(place: number, name: string, score: number): void {
        const placeText = new Text(place.toString(), new LeaderboardItemTextStyle(0xffffff));
        placeText.anchor.y = 0.5;
        placeText.x = -this.width / 2 - placeText.width - this.elementsOffset;
        this.addChild(placeText);

        const namePlateText = new Text(name, new LeaderboardItemTextStyle(0x333333));
        namePlateText.anchor.y = 0.5;
        namePlateText.x = -this.width / 2 + this.elementsOffset;
        this.addChild(namePlateText);

        const scorePlate = new Sprite(this.gameSceneUiResourcePacakge.midleader_scores_plate);
        scorePlate.anchor.y = 0.5;
        scorePlate.x = this.width / 2 + this.elementsOffset;
        this.addChild(scorePlate);

        const scorePlateText = new Text(score.toString(), new LeaderboardItemTextStyle(0x333333));
        scorePlateText.anchor.set(0.5, 0.5);
        scorePlateText.x = scorePlate.width / 2;
        scorePlate.addChild(scorePlateText);
    }
}

class PrizeLeaderboardItem extends Sprite {
    private readonly gameSceneUiResourcePacakge: GameSceneUiResourcePackage;
    private readonly namePlatesFromType: Map<PrizeLeaderboardItemType, Texture>;
    private readonly fillsFromType: Map<PrizeLeaderboardItemType, TextStyleFill>;
    private readonly elementsOffset: number = 20;

    public constructor(gameSceneUiResourcePacakge: GameSceneUiResourcePackage, place: number, name: string, score: number) {
        super();
        this.gameSceneUiResourcePacakge = gameSceneUiResourcePacakge;
        this.namePlatesFromType = new Map([
            [PrizeLeaderboardItemType.Gold, gameSceneUiResourcePacakge.place_1],
            [PrizeLeaderboardItemType.Silver, gameSceneUiResourcePacakge.place_2],
            [PrizeLeaderboardItemType.Bronze, gameSceneUiResourcePacakge.place_3]
        ]);
        this.fillsFromType = new Map([
            [PrizeLeaderboardItemType.Gold, 0xc26102],
            [PrizeLeaderboardItemType.Silver, 0x215db0],
            [PrizeLeaderboardItemType.Bronze, 0x8b1b01]
        ]);
        this.texture = this.namePlatesFromType.get(place);
        this.anchor.set(0.5, 0.5);
        this.create(place, name, score);
    }

    private create(place: number, name: string, score: number): void {
        const namePlateText = new Text(name, new LeaderboardItemTextStyle(this.fillsFromType.get(place)));
        namePlateText.anchor.y = 0.5;
        namePlateText.x = -this.width / 2 + 100;
        this.addChild(namePlateText);

        const scorePlate = new Sprite(this.gameSceneUiResourcePacakge.highleader_scores_plate);
        scorePlate.anchor.y = 0.5;
        scorePlate.x = this.width / 2 + this.elementsOffset;
        this.addChild(scorePlate);

        const scorePlateText = new Text(score.toString(), new LeaderboardItemTextStyle(this.fillsFromType.get(place)));
        scorePlateText.anchor.set(0.5, 0.5);
        scorePlateText.x = scorePlate.width / 2;
        scorePlate.addChild(scorePlateText);
    }
}

enum PrizeLeaderboardItemType {
    Gold,
    Silver,
    Bronze
}

class LeaderboardItemTextStyle extends TextStyle {
    public constructor(fill: TextStyleFill) {
        super({
            align: "center",
            fontFamily: "Regular",
            fontSize: 40,
            fill: fill
        });
    }
}

class LeadboardSliderTextStyle extends TextStyle {
    public constructor() {
        super({
            align: "center",
            fontFamily: "Regular",
            fontSize: 60,
            fill: 0xff6801,
            dropShadow: true,
            dropShadowDistance: 5,
            dropShadowAlpha: 0.5
        });
    }
}

enum LeadboardSliderStates {
    All,
    Month,
    Week
}

class TestRecords {
    private readonly recordsCount = 10;
    public allTimeRecords: Record[];
    public monthRecords: Record[];
    public weekRecords: Record[];

    public constructor() {
        this.allTimeRecords = new Array<Record>(this.recordsCount);
        this.monthRecords = new Array<Record>(this.recordsCount);
        this.weekRecords = new Array<Record>(this.recordsCount);
        for (let i = 0; i < this.recordsCount; i++) {
            const allTimeRecord = new Record();
            allTimeRecord.name = `ATUser ${i}`;
            allTimeRecord.score = this.recordsCount - i;
            this.allTimeRecords[i] = allTimeRecord;

            const monthRecord = new Record();
            monthRecord.name = `MUser ${i}`;
            monthRecord.score = this.recordsCount - i;
            this.monthRecords[i] = monthRecord;

            const weekRecord = new Record();
            weekRecord.name = `WUser ${i}`;
            weekRecord.score = this.recordsCount - i;
            this.weekRecords[i] = weekRecord;
        }
    }
}

class Record {
    public name: string;
    public score: number;
}