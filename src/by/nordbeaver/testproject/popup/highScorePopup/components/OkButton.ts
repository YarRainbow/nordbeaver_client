import { Container } from "pixi.js";
import { ButtonWithSFX } from "../../../scene/gameScene/components/ui/ButtonWithSFX";
import { GameSceneUiResourcePackage } from "../../../scene/gameScene/resources/GameSceneUiResourcePackage";
import { SoundManager } from "../../../soundManager/SoundManager";

export class OkButton extends ButtonWithSFX {
    public constructor(parent: Container, gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager) {
        super(parent, gameSceneUiResourcePackage.ok_button_active, gameSceneUiResourcePackage.ok_button_hover, gameSceneUiResourcePackage.ok_button_press, soundManager);
    }
}