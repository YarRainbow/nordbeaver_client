import MiniSignal from "mini-signals";
import { INetworkClient } from "../../network/INetwortClient";
import { DefaultMessage } from "../../network/messages/DefaultMessage";
import { ErrorMessage, ErrorMessageCode } from "../../network/messages/ErrorMessage";

export class ErrorModel {
    public readonly ErrorReceived: MiniSignal;
    private _errorCode: ErrorMessageCode;
    public get errorCode(): ErrorMessageCode {
        return this._errorCode;
    }

    public constructor(networkClient: INetworkClient) {
        this.ErrorReceived = new MiniSignal();
        networkClient.onMessage.add(this.onMessage, this);
    }

    private onMessage(roomId: number, message: DefaultMessage): void {
        if (message instanceof ErrorMessage) {
            this._errorCode = message.errorCode;
            this.ErrorReceived.dispatch();
        }
    }
}