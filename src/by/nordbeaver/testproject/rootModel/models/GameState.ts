export enum GameState {
    Created,
    CountdownToStart,
    Started,
    Finished
}