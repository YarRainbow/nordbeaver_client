import MiniSignal from "mini-signals";
import { INetworkClient } from "../../network/INetwortClient";
import { DefaultMessage } from "../../network/messages/DefaultMessage";
import { GameStateMessage } from "../../network/messages/GameStateMessage";

export class GameStateTimeModel {
    private previousStateTime: number;
    public get gameStateTime(): number {
        return this.previousStateTime;
    }
    public readonly GameStateTimeUpdated: MiniSignal;

    public constructor(networkClient: INetworkClient) {
        this.GameStateTimeUpdated = new MiniSignal();
        networkClient.onMessage.add(this.onMessage, this);
    }

    private onMessage(roomId: number, message: DefaultMessage): void {
        if (message instanceof GameStateMessage) {
            if (this.previousStateTime != message.stateTime) {
                this.previousStateTime = message.stateTime;
            }
            this.GameStateTimeUpdated.dispatch();
        }
    }
}