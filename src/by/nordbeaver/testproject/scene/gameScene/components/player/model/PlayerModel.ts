import MiniSignal from "mini-signals";
import { DefaultMessage } from "../../../../../network/messages/DefaultMessage";
import { GameStateMessage } from "../../../../../network/messages/GameStateMessage";
import { NetworkClient } from "../../../../../network/NetworkClient";
import { ByteReader } from "../../../../../network/serialization/ByteReader";

export class PlayerModel {
    public readonly onPlayerModelUpdateSignal: MiniSignal;

    public get UserId(): number {
        return this.userId;
    }
    private userId: number;

    public get Position(): number {
        return this.position;
    }
    private position: number;

    public get Score(): number {
        return this.score;
    }
    private score: number;

    public get Nickname(): string {
        return this.nickname;
    }
    private nickname: string;

    public constructor(networkClient: NetworkClient) {
        this.onPlayerModelUpdateSignal = new MiniSignal();
        networkClient.onMessage.add(this.onMessage, this);
    }

    private onMessage(roomId: number, message: DefaultMessage): void {
        if (message instanceof GameStateMessage) {
            const reader: ByteReader = new ByteReader(Buffer.from(message.player));
            this.userId = reader.ReadInt();
            this.position = reader.ReadInt();
            this.score = reader.ReadInt();
            this.nickname = reader.ReadString();
        }
        this.onPlayerModelUpdateSignal.dispatch();
    }
}