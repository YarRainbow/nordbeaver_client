import { Container } from "pixi.js";
import { SoundManager } from "../../../../soundManager/SoundManager";
import { ButtonWithSFX } from "../../components/ui/ButtonWithSFX";
import { GameSceneUiResourcePackage } from "../../resources/GameSceneUiResourcePackage";

export class PauseButton extends ButtonWithSFX {
    public constructor(parent: Container, gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager) {
        super(parent, gameSceneUiResourcePackage.btn_pause_active, gameSceneUiResourcePackage.btn_pause_hover, gameSceneUiResourcePackage.btn_pause_press, soundManager);
    }
}