import { MiniSignalBinding } from "mini-signals";
import { IDestroyOptions, Sprite, Text, TextStyle } from "pixi.js";
import { PlayerModel } from "../../components/player/model/PlayerModel";
import { GameSceneUiResourcePackage } from "../../resources/GameSceneUiResourcePackage";

export class CoinsCounter extends Sprite {
    private readonly gameSceneUiResourcePackage: GameSceneUiResourcePackage;
    private readonly playerModel: PlayerModel;
    private readonly playerModelUpdatedBinding: MiniSignalBinding;
    private label: Text;

    public constructor(gameSceneUiResourcePackage: GameSceneUiResourcePackage, playerModel: PlayerModel) {
        super(gameSceneUiResourcePackage.coin_score_plate);
        this.gameSceneUiResourcePackage = gameSceneUiResourcePackage;
        this.playerModel = playerModel;
        this.playerModelUpdatedBinding = playerModel.onPlayerModelUpdateSignal.add(this.onPlayerModelUpdated, this);
        this.build();
    }

    private build(): void {
        const icon = new Sprite(this.gameSceneUiResourcePackage.collect_coin_icon);
        icon.anchor.set(0.5, 0.5);
        icon.y = this.height / 2;
        this.addChild(icon);

        this.label = new Text("0", new CoinsCounterTextStyle());
        this.label.anchor.set(0.5, 0.5);
        this.label.x = this.width / 2;
        this.label.y = this.height / 2;
        this.addChild(this.label);
    }

    private onPlayerModelUpdated(): void {
        this.label.text = this.playerModel.Score.toString();
    }

    public override destroy(options?: boolean | IDestroyOptions): void {
        super.destroy(options);
        this.playerModel.onPlayerModelUpdateSignal.detach(this.playerModelUpdatedBinding);
    }
}

class CoinsCounterTextStyle extends TextStyle {
    public constructor() {
        super({
            align: "center",
            fontFamily: "Regular",
            fontSize: 50,
            fill: 0xFFFFFF,
        });
    }
}