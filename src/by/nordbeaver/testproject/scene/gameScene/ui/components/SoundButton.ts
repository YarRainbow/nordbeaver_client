import { Container } from "pixi.js";
import { ToggleButton } from "../../../../../ui/widgets/ToggleButton";
import { SoundManager } from "../../../../soundManager/SoundManager";
import { GameSceneUiResourcePackage } from "../../resources/GameSceneUiResourcePackage";

export class SoundButton extends ToggleButton {
    private readonly soundManager: SoundManager;

    public constructor(parent: Container, gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager) {
        super(parent, gameSceneUiResourcePackage.btn_sound_1_active, gameSceneUiResourcePackage.btn_sound_0_active,
            gameSceneUiResourcePackage.btn_sound_1_hover, gameSceneUiResourcePackage.btn_sound_0_hover,
            gameSceneUiResourcePackage.btn_sound_1_press, gameSceneUiResourcePackage.btn_sound_0_press);
        this.soundManager = soundManager;
    }

    protected override onPointerUp(): void {
        super.onPointerUp();
        this.soundManager.enable(this.isActive);
    }
}