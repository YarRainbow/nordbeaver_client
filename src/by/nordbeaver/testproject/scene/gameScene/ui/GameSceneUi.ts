import { Container } from "pixi.js";
import { Game } from "../../../../Game";
import { SoundManager } from "../../../soundManager/SoundManager";
import { PlayerModel } from "../components/player/model/PlayerModel";
import { GameSceneUiResourcePackage } from "../resources/GameSceneUiResourcePackage";
import { CoinsCounter } from "./components/CoinsCounter";
import { FullScreenButton } from "./components/FullScreenButton";
import { PauseButton } from "./components/PauseButton";
import { SoundButton } from "./components/SoundButton";

export class GameSceneUi extends Container {
    private readonly gameSceneUiResourcePackage: GameSceneUiResourcePackage;
    private readonly soundManager: SoundManager;
    private readonly playerModel: PlayerModel;
    private readonly uiElementPositionOffsetX: number = 50;
    private readonly uiElementPositionOffsetY: number = 20;
    private readonly paddingBetweenNavigationButtons: number = 10;

    public constructor(gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager, playerModel: PlayerModel) {
        super();
        this.gameSceneUiResourcePackage = gameSceneUiResourcePackage;
        this.soundManager = soundManager;
        this.playerModel = playerModel;
        this.build();
    }

    private build(): void {
        this.createCoinsCounter();
        this.createNavigationButtons();
    }

    private createCoinsCounter(): void {
        const coinsCounter = new CoinsCounter(this.gameSceneUiResourcePackage, this.playerModel);
        coinsCounter.x = this.uiElementPositionOffsetX + 25;
        coinsCounter.y = this.uiElementPositionOffsetY + 25;
        this.addChild(coinsCounter);
    }

    private createNavigationButtons(): void {
        const pauseButton = new PauseButton(this, this.gameSceneUiResourcePackage, this.soundManager);
        pauseButton.anchor.set(1, 0);
        pauseButton.x = Game.WIDTH - this.uiElementPositionOffsetX;
        pauseButton.y = this.uiElementPositionOffsetY;

        const soundButton = new SoundButton(this, this.gameSceneUiResourcePackage, this.soundManager);
        soundButton.anchor.set(1, 0);
        soundButton.x = pauseButton.x - pauseButton.width - this.paddingBetweenNavigationButtons;
        soundButton.y = this.uiElementPositionOffsetY;

        const fullScreenButton = new FullScreenButton(this, this.gameSceneUiResourcePackage, this.soundManager);
        fullScreenButton.anchor.set(1, 0);
        fullScreenButton.x = soundButton.x - soundButton.width - this.paddingBetweenNavigationButtons;
        fullScreenButton.y = this.uiElementPositionOffsetY;
    }
}