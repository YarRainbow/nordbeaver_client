import { BaseView } from "../../../../scene/viewable/BaseView";
import { GameSceneViewController } from "../controller/GameSceneViewController";
import { GameSceneUi } from "../ui/GameSceneUI";
import { Gamefield } from "../components/gamefield/Gamefield";
import { GameStateModel } from "../../../rootModel/models/GameStateModel";
import { IDestroyOptions } from "pixi.js";
import { MiniSignalBinding } from "mini-signals";
import { GameState } from "../../../rootModel/models/GameState";

export class DefaultGameView extends BaseView {
    private readonly controller: GameSceneViewController;
    private readonly gameStateModel: GameStateModel;
    private readonly gameStateModelBinding: MiniSignalBinding;

    public constructor(controller: GameSceneViewController, gameStateModel: GameStateModel) {
        super();
        this.controller = controller;
        this.gameStateModel = gameStateModel;
        this.gameStateModelBinding = gameStateModel.GameStateUpdated.add(this.onGameStateModelUpdated, this);
        this.start();
    }

    public start(): void {
        const gamefield: Gamefield = this.controller.createGamefield();
        this.addChild(gamefield);

        const gameSceneUiContainer: GameSceneUi = this.controller.createGameSceneUi();
        this.addChild(gameSceneUiContainer);
    }

    private onGameStateModelUpdated(): void {
        switch(this.gameStateModel.gameState) {
            case GameState.Created: {
                this.controller.onGameStateCreated();
                break;
            }
            case GameState.CountdownToStart: {
                this.controller.onGameStateCountdownToStart();
                break;
            }
            case GameState.Started: {
                this.controller.onGameStateStarted();
                break;
            }
            case GameState.Finished: {
                this.controller.onGameStateFinished();
                break;
            }
            default: {
                throw ("Error: Unknown Game State");
            }
        }
    }

    public override destroy(options?: boolean | IDestroyOptions): void {
        super.destroy(options);
        this.gameStateModel.GameStateUpdated.detach(this.gameStateModelBinding);
    }
}