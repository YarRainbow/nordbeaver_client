import { ResourceManager } from "../../../../resources/ResourceManager";
import { GameSceneResourcePackage } from "../../gameScene/resources/GameSceneResourcePackage";
import { GameSceneUiResourcePackage } from "../../gameScene/resources/GameSceneUiResourcePackage";
import { ResourcesConfigModel } from "../model/ResourcesConfigModel";

export class PreloaderSceneController {
    public async load(resourceManager: ResourceManager): Promise<ResourcesConfigModel> {
        const resourcesConfigModel = new ResourcesConfigModel();
        resourcesConfigModel.gameSceneResourcePackage = await resourceManager.getResourcePackage(GameSceneResourcePackage);
        resourcesConfigModel.gameSceneUiResourcePackage = await resourceManager.getResourcePackage(GameSceneUiResourcePackage);
        return Promise.resolve(resourcesConfigModel);
    }
}