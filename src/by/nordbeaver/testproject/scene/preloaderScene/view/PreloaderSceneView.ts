import { StartupConfig } from "../../../../configs/StartupConfig";
import { ResourceManager } from "../../../../resources/ResourceManager";
import { BaseView } from "../../../../scene/viewable/BaseView";
import { ViewableOperationControls } from "../../../../scene/viewable/Viewable";
import { NetworkClient } from "../../../network/NetworkClient";
import { PreloaderResourcesPackage } from "../resources/PreloaderResourcesPackage";
import { PreloaderSceneController } from "../controller/PreloaderSceneController";
import { PreloaderConfigModel } from "../model/PreloaderConfigModel";
import { ResourcesConfigModel } from "../model/ResourcesConfigModel";
import { Sprite } from "pixi.js";
import { Game } from "../../../../Game";

export class PreloaderSceneView extends BaseView {
    private controller: PreloaderSceneController;
    private resourceManager: ResourceManager;
    private networkClient: NetworkClient;
    private startupConfig: StartupConfig;
    private operationControls: ViewableOperationControls<PreloaderConfigModel>;
    private preloaderResourcesPackage: PreloaderResourcesPackage;

    public constructor(controller: PreloaderSceneController, resourceManager: ResourceManager, networkClient: NetworkClient, startupConfig: StartupConfig, operationControls: ViewableOperationControls<PreloaderConfigModel>, preloaderResourcesPackage: PreloaderResourcesPackage) {
        super();
        this.controller = controller;
        this.resourceManager = resourceManager;
        this.networkClient = networkClient;
        this.startupConfig = startupConfig;
        this.operationControls = operationControls;
        this.preloaderResourcesPackage = preloaderResourcesPackage;
        this.start();
    }

    public start(): void {
        const background = new Sprite(this.preloaderResourcesPackage.bg_preloader);
        background.width = Game.WIDTH;
        background.height = Game.HEIGHT;
        this.addChild(background);

        const bunny = new Sprite(this.preloaderResourcesPackage.mi_bunny);
        bunny.anchor.set(0.5, 0.5);
        bunny.x = Game.WIDTH / 2;
        bunny.y = Game.HEIGHT / 2;
        this.addChild(bunny);

        this.controller.load(this.resourceManager).then((resourcesConfigModel: ResourcesConfigModel) => {
            const preloaderConfigModel = new PreloaderConfigModel();
            preloaderConfigModel.resources = resourcesConfigModel;
            preloaderConfigModel.networkClient = this.networkClient;
            preloaderConfigModel.startupConfig = this.startupConfig;
            this.operationControls.complete(preloaderConfigModel); 
        })
    }

}