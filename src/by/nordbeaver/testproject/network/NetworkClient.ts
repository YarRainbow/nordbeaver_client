import MiniSignal from "mini-signals"
import { INetworkClient } from "./INetwortClient";
import { DefaultMessage } from "./messages/DefaultMessage";
import { ErrorMessage, ErrorMessageCode } from "./messages/ErrorMessage";
import { JoinRoomMessage } from "./messages/JoinRoomMessage";
import { ReadyToPlayMessage } from "./messages/ReadyToPlayMessage";
import { MessageTypeResolver } from "./MessageTypeResolver";
import { NetworkConfig } from "./NetworkConfig";

export class NetworkClient implements INetworkClient {
    public static readonly UPDATE_MSG_PER_SECOND: number = 10;
    public static readonly USER_INPUT_SEND_INTERVAL_MS: number = 1000 / NetworkClient.UPDATE_MSG_PER_SECOND;
    public static readonly MAX_DIFF_POSITION_WITH_SERVER = 2;
    private static readonly IPRegex: RegExp = /^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/;
    public onMessage: MiniSignal;
    private socket: WebSocket;
    private readonly config: NetworkConfig;
    private readonly roomId: number;
    private onConnectedResolver: (v: void) => void;
    private onConnectedReject: (reason: any) => void;
    private messageTypeResolver: MessageTypeResolver;

    public constructor(config: NetworkConfig, roomId: number) {
        this.onMessage = new MiniSignal();
        this.config = config;
        this.roomId = roomId;
        this.messageTypeResolver = new MessageTypeResolver();
    }

    public async connect(): Promise<void> {
        return new Promise((resolve, reject) => {
            const connectURL = this.resolveConnectUrl();
            this.socket = new WebSocket(connectURL);
            this.socket.binaryType = "arraybuffer";
            this.socket.onopen = (e: Event) => this.onConnect(e);
            this.socket.onmessage = (msg: MessageEvent<Uint8Array>) => this.onReceive(msg);
            this.socket.onerror = (e: Event) => this.onError(e);
            this.socket.onclose = (e: CloseEvent) => this.onClose(e);
            this.onConnectedResolver = resolve;
            this.onConnectedReject = reject;
        });
    }

    private resolveConnectUrl(): string {
        const protocol = this.config.secure ? "wss" : "ws";
        if (NetworkClient.IPRegex.test(this.config.host)) {
            return `${protocol}://${this.config.host}:${this.config.port}`;
        }
        else {
            return `${protocol}://${this.config.host}`;
        }
    }

    private onConnect(event: Event): void {
        this.onConnectedResolver();
    }

    private onReceive(rawMessage: MessageEvent<Uint8Array>): void {
        const buffer = Buffer.from(rawMessage.data);
        const message = this.messageTypeResolver.TypeCodeToType(buffer[DefaultMessage.MESSAGE_TYPE_POSITION]);
        const roomId = buffer.readInt32LE(DefaultMessage.MESSAGE_ROOMID_POSITION);
        message.FromBytes(rawMessage.data);
        this.onMessage.dispatch(roomId, message);
    }

    private onError(e: Event): void {
        console.error(e.type);
    }

    private onClose(e: CloseEvent): void {
        const errorMessage = new ErrorMessage();
        errorMessage.errorCode = ErrorMessageCode.GameFinished;
        this.onMessage.dispatch(this.roomId, errorMessage);
    }

    public send(message: DefaultMessage): void {
        if (this.socket.readyState != WebSocket.OPEN) {
            return;
        }
        const messageBytes = message.ToBytes(this.messageTypeResolver, this.roomId);
        this.socket.send(messageBytes);
    }

    public joinRoomRequest(userId: number, battleId: number): void {
        const joinRoomMessage = new JoinRoomMessage();
        joinRoomMessage.battleId = battleId;
        joinRoomMessage.userId = userId;
        this.send(joinRoomMessage);
    }

    public readyRequest(): void {
        this.send(new ReadyToPlayMessage());
    }
} 