import { ByteReader } from "../serialization/ByteReader";
import { ByteWriter } from "../serialization/ByteWriter";
import { DefaultMessage } from "./DefaultMessage";

export class GameConfigMessage extends DefaultMessage {
    public allTimeRecords: Uint8Array;
    public monthRecords: Uint8Array;
    public weekRecords: Uint8Array;

    protected GetMessageSize(): number {
        return 8 + this.allTimeRecords.length + this.monthRecords.length + this.weekRecords.length;
    }

    protected WriteMessageBytes(byteWriter: ByteWriter): void {
        byteWriter.WriteByteArray(this.allTimeRecords);
        byteWriter.WriteByteArray(this.monthRecords);
        byteWriter.WriteByteArray(this.weekRecords);
    }

    public FromBytes(bytes: Uint8Array): void {
        const reader: ByteReader = new ByteReader(Buffer.from(bytes), DefaultMessage.MESSAGE_HEADER_SIZE);
        this.allTimeRecords = reader.ReadByteArray();
        this.monthRecords = reader.ReadByteArray();
        this.weekRecords = reader.ReadByteArray();
    }

}