import { ByteReader } from "../serialization/ByteReader";
import { ByteWriter } from "../serialization/ByteWriter";
import { DefaultMessage } from "./DefaultMessage";

export class JoinRoomMessage extends DefaultMessage 
{
    public userId: number;
    public battleId: number;
    
    protected GetMessageSize(): number {
        return 8;
    }

    protected WriteMessageBytes(byteWriter: ByteWriter): void {
        byteWriter.WriteInt(this.userId);
        byteWriter.WriteInt(this.battleId);
    }

    public FromBytes(bytes: Uint8Array): void {
        const reader:ByteReader = new ByteReader(Buffer.from(bytes), DefaultMessage.MESSAGE_HEADER_SIZE);
        this.userId = reader.ReadInt();
        this.battleId = reader.ReadInt();
    }

}