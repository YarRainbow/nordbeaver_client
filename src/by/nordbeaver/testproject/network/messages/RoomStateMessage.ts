import { ByteReader } from "../serialization/ByteReader";
import { ByteWriter } from "../serialization/ByteWriter";
import { DefaultMessage } from "./DefaultMessage";

export class RoomStateMessage extends DefaultMessage {
    public state: number;
    public stateTimerSec: number;

    protected GetMessageSize(): number {
        return 2;
    }

    protected WriteMessageBytes(byteWriter: ByteWriter): void {
        byteWriter.WriteByte(this.state);
        byteWriter.WriteByte(this.stateTimerSec);
    }

    public FromBytes(bytes: Uint8Array): void {
        const reader: ByteReader = new ByteReader(Buffer.from(bytes), DefaultMessage.MESSAGE_HEADER_SIZE);
        this.state = reader.ReadByte();
        this.stateTimerSec = reader.ReadByte();
    }
}

export enum RoomStateId
{
    WaitingForPlayers,
    StartupCountdownStarted,
    MindplaysStartGame,
    GameStarted,
    GameFinished,
    RewardsGranted
}