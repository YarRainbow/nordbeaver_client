import { ByteWriter } from "../serialization/ByteWriter";
import { DefaultMessage } from "./DefaultMessage";

export class UserInputMessage extends DefaultMessage {
    protected GetMessageSize(): number {
        return 0;
    }

    public override FromBytes(bytes: Uint8Array): void {
    }

    protected WriteMessageBytes(byteWriter: ByteWriter): void {
    }
}