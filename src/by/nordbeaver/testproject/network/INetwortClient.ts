import MiniSignal from "mini-signals";
import { DefaultMessage } from "./messages/DefaultMessage";

export interface INetworkClient {
    onMessage: MiniSignal;
    send(message: DefaultMessage): void;
}