export class NetworkConfig {
    public readonly host: string = "127.0.0.1";
    public readonly port: string = "8555";
    public readonly secure: boolean = false;
}