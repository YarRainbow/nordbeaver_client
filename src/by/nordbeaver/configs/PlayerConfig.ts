export class PlayerConfig
{
    public readonly UserInputSendIntervalMs: number = 100;
    public readonly UPDATE_MSG_PER_SECOND: number = 1000 / this.UserInputSendIntervalMs;
}