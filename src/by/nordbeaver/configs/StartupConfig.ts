export class StartupConfig {
    public readonly userId : number;
    public readonly roomId : number;
    public readonly battleId: number;

    public constructor() {
        const url : string = window.location.search;
        const urlParams = new URLSearchParams(url);
        if (!urlParams.has('user_id')) {
            throw new Error("No user_id provided in request URL.");
        }
        if (!urlParams.has('room_id')) {
            throw new Error("No room_id provided in request URL.");
        }
        if (!urlParams.has('battle_id')) {
            throw new Error("No battle_id provided in request URL.");
        }
        this.userId = parseInt(urlParams.get('user_id'));
        this.roomId = parseInt(urlParams.get('room_id'));
        this.battleId = parseInt(urlParams.get('battle_id'));
    }
}